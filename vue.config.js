module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "~@/plugins/assets/styles/scss/index.scss";`
      },
      less: {
        javascriptEnabled: true
      }
    }
  }
}
