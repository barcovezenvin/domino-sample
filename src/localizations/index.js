import Vue from "vue";
import VueI18n from "vue-i18n";
import en from "./en";
import zh from "./zh";
import id from "./id";
import vi from "./vi";
import th from "./th";
import ja from "./ja";
import ko from "./ko";
import my from "./my";

Vue.use(VueI18n);

const messages = {
  en,
  zh,
  id,
  vi,
  th,
  ja,
  ko,
  my
};

const i18n = new VueI18n({
  locale: "en",
  fallbackLocale: "en",
  messages
});

export default i18n;
