import Vue from "vue";
import Barcove from "@/plugins/Barcove";
import i18n from "./localizations";
import App from "./App.vue";

Vue.use(Barcove);

Vue.config.productionTip = false;

new Vue({
  i18n,
  ...App
  // render: h => h(App)
}).$mount("#app");
