
import Domino from "./components/domino/Domino"
import "iview/dist/styles/iview.css"
import "./assets/styles/less/index.less"

const Max3D = {}

Max3D.install = function(Vue) {
  Vue.component("domino", Domino)
}

export default Max3D
