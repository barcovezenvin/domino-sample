/***************
 * Action types
 ***************/
const betlotInitialState = () => {
  return {
    isShowingResult: false,
    stake: "",
    rules: null,
    betting: false,
    // responseMessage: null,
    refNo: "",
    setStakeTimeout: null,
    closeResponsePopperTimeout: null,
    dice1SelectedType: "o11",
    dice2SelectedType: "o21",
    dice3SelectedType: "o31",
    dice4SelectedType: "o41",
    dice5SelectedType: "o51",
    dice6SelectedType: "o61"
  };
};

const openWindowSize = [800, 600];

// Constant
const max_dice_betTypes = {
  o1: { code: "o1", betID: 1, gameTypeID: 1 },
  o2: { code: "o2", betID: 2, gameTypeID: 1 },
  o3: { code: "o3", betID: 3, gameTypeID: 1 },
  o4: { code: "o4", betID: 4, gameTypeID: 2 },
  o5: { code: "o5", betID: 5, gameTypeID: 2 },
  o11: { code: "o11", betID: 11, gameTypeID: 3 },
  o12: { code: "o12", betID: 12, gameTypeID: 3 },
  o13: { code: "o13", betID: 13, gameTypeID: 3 },
  o21: { code: "o21", betID: 21, gameTypeID: 3 },
  o22: { code: "o22", betID: 22, gameTypeID: 3 },
  o23: { code: "o23", betID: 23, gameTypeID: 3 },
  o31: { code: "o31", betID: 31, gameTypeID: 3 },
  o32: { code: "o32", betID: 32, gameTypeID: 3 },
  o33: { code: "o33", betID: 33, gameTypeID: 3 },
  o41: { code: "o41", betID: 41, gameTypeID: 3 },
  o42: { code: "o42", betID: 42, gameTypeID: 3 },
  o43: { code: "o43", betID: 43, gameTypeID: 3 },
  o51: { code: "o51", betID: 51, gameTypeID: 3 },
  o52: { code: "o52", betID: 52, gameTypeID: 3 },
  o53: { code: "o53", betID: 53, gameTypeID: 3 },
  o61: { code: "o61", betID: 61, gameTypeID: 3 },
  o62: { code: "o62", betID: 62, gameTypeID: 3 },
  o63: { code: "o63", betID: 63, gameTypeID: 3 },
  o104: { code: "o104", betID: 104, gameTypeID: 4 },
  o105: { code: "o105", betID: 105, gameTypeID: 4 },
  o106: { code: "o106", betID: 106, gameTypeID: 4 },
  o107: { code: "o107", betID: 107, gameTypeID: 4 },
  o108: { code: "o108", betID: 108, gameTypeID: 4 },
  o109: { code: "o109", betID: 109, gameTypeID: 4 },
  o110: { code: "o110", betID: 110, gameTypeID: 4 },
  o111: { code: "o111", betID: 111, gameTypeID: 4 },
  o112: { code: "o112", betID: 112, gameTypeID: 4 },
  o113: { code: "o113", betID: 113, gameTypeID: 4 },
  o114: { code: "o114", betID: 114, gameTypeID: 4 },
  o115: { code: "o115", betID: 115, gameTypeID: 4 },
  o116: { code: "o116", betID: 116, gameTypeID: 4 },
  o117: { code: "o117", betID: 117, gameTypeID: 4 }
};

const hi_lo_betTypes = {
  o1: { code: "o1", betID: 1, gameTypeID: 1 },
  o2: { code: "o2", betID: 2, gameTypeID: 1 },
  o3: { code: "o3", betID: 3, gameTypeID: 2 },
  o4: { code: "o4", betID: 4, gameTypeID: 2 },
  o5: { code: "o5", betID: 5, gameTypeID: 3 },
  o6: { code: "o6", betID: 6, gameTypeID: 4 },
  o7: { code: "o7", betID: 7, gameTypeID: 5 },
  o101: { code: "o101", betID: 101, gameTypeID: 6 },
  o102: { code: "o102", betID: 102, gameTypeID: 7 },
  o103: { code: "o103", betID: 103, gameTypeID: 8 },
  o104: { code: "o104", betID: 104, gameTypeID: 9 },
  o105: { code: "o105", betID: 105, gameTypeID: 10 },
  o106: { code: "o106", betID: 106, gameTypeID: 11 },
  o107: { code: "o107", betID: 107, gameTypeID: 12 },
  o108: { code: "o108", betID: 108, gameTypeID: 13 },
  o109: { code: "o109", betID: 109, gameTypeID: 14 },
  o110: { code: "o110", betID: 110, gameTypeID: 15 },
  o111: { code: "o111", betID: 111, gameTypeID: 16 },
  o112: { code: "o112", betID: 112, gameTypeID: 17 },
  o113: { code: "o113", betID: 113, gameTypeID: 18 }
};

const domino_betTypes = {
  o1: { code: "o1", betID: 1, gameTypeID: 1 },
  o2: { code: "o2", betID: 2, gameTypeID: 1 },
  o3: { code: "o3", betID: 3, gameTypeID: 1 },
  o4: { code: "o4", betID: 4, gameTypeID: 2 },
  o5: { code: "o5", betID: 5, gameTypeID: 2 },
  o6: { code: "o6", betID: 6, gameTypeID: 3 },
  o7: { code: "o7", betID: 7, gameTypeID: 3 },
  o8: { code: "o8", betID: 8, gameTypeID: 4 },
  o9: { code: "o9", betID: 9, gameTypeID: 4 },
  o10: { code: "o10", betID: 10, gameTypeID: 5 },
  o11: { code: "o11", betID: 11, gameTypeID: 5 }
};

const maxracing_betTypes = {
  o1: { code: "o1", betID: 1, gameTypeID: 1 },
  o2: { code: "o2", betID: 2, gameTypeID: 2 },
  o3: { code: "o3", betID: 3, gameTypeID: 2 },
  o4: { code: "o4", betID: 4, gameTypeID: 3 },
  o5: { code: "o5", betID: 5, gameTypeID: 3 },
  o6: { code: "o6", betID: 6, gameTypeID: 4 },
  o7: { code: "o7", betID: 7, gameTypeID: 4 },
  o8: { code: "o8", betID: 8, gameTypeID: 5 },
  o9: { code: "o9", betID: 9, gameTypeID: 5 },
  o10: { code: "o10", betID: 10, gameTypeID: 6 },
  o11: { code: "o11", betID: 11, gameTypeID: 6 },
  o12: { code: "o12", betID: 12, gameTypeID: 6 },
  o13: { code: "o13", betID: 13, gameTypeID: 6 },
  o14: { code: "o14", betID: 14, gameTypeID: 6 }
};

const fantan_betTypes = {
  o1: { code: "o1", betID: 1, gameTypeID: 1 },
  o2: { code: "o2", betID: 2, gameTypeID: 2 },
  o3: { code: "o3", betID: 3, gameTypeID: 3 },
  o4: { code: "o4", betID: 4, gameTypeID: 4 },
  o5: { code: "o5", betID: 5, gameTypeID: 5 },
  o6: { code: "o6", betID: 6, gameTypeID: 6 },
  o7: { code: "o7", betID: 7, gameTypeID: 7 }
};

const thaihilo_betTypes = {
  o1: { code: "o1", betID: 1, gameTypeID: 1 },
  o2: { code: "o2", betID: 2, gameTypeID: 1 },
  o3: { code: "o3", betID: 3, gameTypeID: 2 },
  o4: { code: "o4", betID: 4, gameTypeID: 3 },
  o5: { code: "o5", betID: 5, gameTypeID: 4 },
  o6: { code: "o6", betID: 6, gameTypeID: 5 },
  o7: { code: "o7", betID: 7, gameTypeID: 6 },
  o8: { code: "o8", betID: 8, gameTypeID: 7 },
  o9: { code: "o9", betID: 9, gameTypeID: 8 },

  o11: { code: "o11", betID: 11, gameTypeID: 9 },
  o21: { code: "o21", betID: 12, gameTypeID: 9 },
  o31: { code: "o31", betID: 13, gameTypeID: 9 },
  o41: { code: "o41", betID: 14, gameTypeID: 9 },
  o51: { code: "o51", betID: 15, gameTypeID: 9 },
  o61: { code: "o61", betID: 16, gameTypeID: 9 },

  //Double Number
  o70: { code: "o70", betID: 17, gameTypeID: 10 },
  o71: { code: "o71", betID: 18, gameTypeID: 10 },
  o72: { code: "o72", betID: 19, gameTypeID: 10 },
  o73: { code: "o73", betID: 20, gameTypeID: 10 },
  o74: { code: "o74", betID: 21, gameTypeID: 10 },
  o75: { code: "o75", betID: 22, gameTypeID: 10 },
  o76: { code: "o76", betID: 23, gameTypeID: 10 },
  o77: { code: "o77", betID: 24, gameTypeID: 10 },
  o78: { code: "o78", betID: 25, gameTypeID: 10 },
  o79: { code: "o79", betID: 26, gameTypeID: 10 },
  o80: { code: "o80", betID: 27, gameTypeID: 10 },
  o81: { code: "o81", betID: 28, gameTypeID: 10 },
  o82: { code: "o82", betID: 29, gameTypeID: 10 },
  o83: { code: "o83", betID: 30, gameTypeID: 10 },
  o84: { code: "o84", betID: 31, gameTypeID: 10 },

  //Triple Number
  o85: { code: "o85", betID: 32, gameTypeID: 11 },
  o86: { code: "o86", betID: 33, gameTypeID: 11 },
  o87: { code: "o87", betID: 34, gameTypeID: 11 },
  o88: { code: "o88", betID: 35, gameTypeID: 11 },
  o89: { code: "o89", betID: 36, gameTypeID: 11 },
  o90: { code: "o90", betID: 36, gameTypeID: 11 },
  o91: { code: "o91", betID: 37, gameTypeID: 11 },
  o92: { code: "o92", betID: 38, gameTypeID: 11 },
};

const languages = [
  {
    name: "English",
    code: "en"
  },
  {
    name: "\u4e2d\u6587",
    code: "zh"
  },
  {
    name: "Bahasa Indonesia",
    code: "id"
  },
  {
    name: "Ti\u1ebfng Vi\u1ec7t",
    code: "vi"
  },
  {
    name: "\u0e44\u0e17\u0e22",
    code: "th"
  },
  {
    name: "\u65e5\u672c\u8a9e",
    code: "ja"
  },
  {
    name: "\ud55c\uad6d\uc5b4",
    code: "ko"
  },
  {
    name: "Burmese",
    code: "my"
  }
];

export {
  betlotInitialState,
  // placeBetUrl,
  // replaceBetUrl,
  // apiUrl,
  // customerUrl,
  // customerBalanceUrl,
  // drawsCheckUpdateUrl,
  // drawResultURL,
  // nextDrawsUrl,
  // checkbetTypeUrl,
  // RebetUrl,
  // trendlotUrl,
  // custBetUrl,
  // langUrl,
  // logoutUrl,
  // forceLogoutUrl,
  // betsUrl,
  // statementUrl,
  // gamehistoryUrl,
  // rulesUrl,
  // notificationUrl,
  openWindowSize,
  hi_lo_betTypes,
  max_dice_betTypes,
  languages,
  domino_betTypes,
  maxracing_betTypes,
  fantan_betTypes,
  thaihilo_betTypes
};
