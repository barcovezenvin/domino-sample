/*
  Note: The general configuration for every game, please make this mixin your
  priority source code to maintain. It should behave the same for all your games,
  it serves the modularization and standardization purpose.

  Refer this link if you are unsure the concept of mixin, https://vuejs.org/v2/guide/mixins.html
*/

import axios from "../configs/axios";
import { Col, Card, Message } from "iview";

const gameMixin = {
  props: ["gameData"],
  components: {
    Col,
    Card,
    Message
  },
  data() {
    return {
      drawID: "",
      drawNo: "",
      countDown: 0,
      closeInterval: 0,
      disallowBetting: false,
      displayDisallowSection: false,
      showDrawingAnimation: false,
      drawingResult: false,
      // Workers for timeout and interval
      worker: {
        game: undefined,
        drawResult: undefined,
        nextResult: undefined,
        roadMap: undefined
      },
      drawResult: {},
      stat: {},
      historicalData: {},
      snacktxt: "",
      snacktype: ""
    };
  },
  watch: {
    // If there is gameData changes detected
    gameData() {
      // If there is no worker, set count down with game data count down
      if (!this.worker.game) {
        this.countDown = this.gameData.countDown;
        this.closeInterval = this.gameData.closeInterval;
        this.drawID = this.gameData.drawID;
        this.drawNo = `${this.gameData.gCode}${this.gameData.drawID}`;

        this.startGameCountDownWorker();
        this.retrieveRoadMap();
      }
    }
  },
  computed: {
    gameCode() {
      return this.gameData.gCode;
    },
    title() {
      return this.gameData.title || "Max Dice 60";
    },
    secondsString() {
      let seconds = this.countDown % 60;

      return seconds.toString().padStart(2, "0");
    },
    minutesString() {
      let minutes = parseInt(this.countDown / 60);

      return minutes.toString().padStart(2, "0");
    }
  },
  mounted() {
    // Reset worker during first mounted
    this.worker.game = undefined;
    this.worker.animation = undefined;
    this.worker.drawResult = undefined;
    this.worker.nextResult = undefined;
    this.worker.roadMap = undefined;

    if (!this.worker.game) {
      this.countDown = this.gameData.countDown;
      this.closeInterval = this.gameData.closeInterval;
      this.drawID = this.gameData.drawID;
      this.drawNo = `${this.gameData.gCode}${this.gameData.drawID}`;
      this.drawResult = this.gameData.prev;
      this.startGameCountDownWorker();
      this.retrieveRoadMap();
    }
  },
  methods: {
    displayErrorMessage(content) {
      Message.error({
        duration: 3,
        content,
        top: 100
      });
    },
    displaySuccessMessage(content) {
      Message.success({
        duration: 3,
        content
      });
    },
    // The main game count down timer worker for header
    startGameCountDownWorker() {
      let vm = this;
      // Set count down worker if not any existed
      if (!this.worker.game) {
        this.worker.game = new Worker("js/worker.js");
        this.worker.game.postMessage(["countdown", this.countDown]);
        this.worker.game.onmessage = function(e) {
          vm.countDown = e.data;
          if (vm.countDown <= vm.closeInterval) {
            vm.disallowBetting = true;
            vm.displayDisallowSection = true;
          }
          // When count timer reach zero
          if (vm.countDown === 0) {
            // Clear decrement for count down
            vm.worker.game.terminate();

            // this.displayDisallowSection = false;
            vm.retrieveDrawingResult();
            // }
          }
        };
      }
    },
    async retrieveDrawingResult() {
      let vm = this;
      this.drawingResult = true;
      try {
        // Start sending request for retrieving drawing result
        let res = await axios.get(
          `${drawResultURL}&gID=${this.gameData.gID}&drawID=${this.drawID}`
        );

        let resData = res.data;
        if (res.data.winlose) {
          this.snacktxt =
            this.gameCode + this.gameData.drawID + " " + res.data.winlose;
          this.snacktype = res.data.snacktype;
        }

        if (!resData.data) {
          setTimeout(() => {
            // Always clear next result worker after time out is ended
            vm.retrieveDrawingResult();
          }, 1000);
        } else {
          this.drawingResult = false;
          this.displayDisallowSection = false;

          if (resData.error) {
            this.forceLogout(1, resData.error);
          } else {
            this.gameData.previousdrawno = this.drawID;
            this.showDrawingAnimation = true;

            this.drawResult = resData.data;
          }

          this.worker.animation = setInterval(() => {
            if (!this.showDrawingAnimation) {
              clearInterval(this.worker.animation);

              if (resData.error) {
                this.forceLogout(1, resData.error);
              } else {
                this.retrieveNextDrawing();
              }
            }
          }, 1000);
        }
      } catch (e) {}
    },
    async retrieveNextDrawing() {
      let vm = this;
      try {
        if (this.snacktxt != "") {
          if (this.snacktype == "2") {
            this.displayErrorMessage(this.snacktxt);
          } else {
            this.displaySuccessMessage(this.snacktxt);
          }
          this.snacktxt = "";
          this.snacktype = "";
        }
        // Start sending request for retrieving next draw info
        let res = await axios.get(`${nextDrawsUrl}?gID=${this.gameData.gID}`);

        let resData = res.data;
        // Reset count down timer
        if (!resData.data) {
          setTimeout(() => {
            // Always clear next result worker after time out is ended
            vm.retrieveNextDrawing();
          }, 1000);
        } else {
          this.countDown = resData.data.countDown;

          // Set new draw id and new draw no
          this.drawID = resData.data.drawID;
          this.gameData.drawID = resData.data.drawID;
          this.drawNo = `${resData.data.gCode}${resData.data.drawID}`;

          // Reset game worker with new count down timer
          delete this.worker.game;
          this.startGameCountDownWorker();

          this.worker.roadMap = setTimeout(async () => {
            // Always clear next result worker after time out is ended
            clearTimeout(this.worker.roadMap);
            delete this.worker.roadMap;

            this.retrieveRoadMap();
          }, 2000);
        }
      } catch (e) {}
    },
    async retrieveRoadMap() {
      try {
        // Start sending request for retrieving next draw info
        let res = await axios.get(
          `${trendlotUrl}?gID=${this.gameData.gID}&dID=${this.drawID}`
        );

        let resData = res.data;

        this.stat = resData.stat;
        this.historicalData = resData.data;
      } catch (e) {}
    },
    gameOnAnimationEnd() {
      this.showDrawingAnimation = false;
      this.disallowBetting = false;
    }
  }
};

export default gameMixin;
