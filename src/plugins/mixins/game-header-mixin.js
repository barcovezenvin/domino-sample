/*
  Note: Remember to create new titleSelector for data object in caller component.
  Then set your game header component title selector.
  You may refer components/max-dice/MaxDiceHeader.vue as an example.
*/

import { TweenLite } from "gsap";
// import "gsap/TextPlugin.js";

const gameHeaderMixin = {
  data() {
    return {
      titleSelector: ""
    };
  },
  props: [
    "title",
    "drawNo",
    "game",
    "gameData",
    "disallowBetting",
    "minutesString",
    "secondsString",
    "gameType"
  ],
  methods:{
    drawAnimation: function(a){
      var c = this;
      var str = this.drawNo.slice(0,a);
      var strlength = this.drawNo.length;
      this.$refs["header-draw-no"].innerText  = str;
      if(a<strlength){
        setTimeout(function(){
          c.drawAnimation(a+1);
        },75);
      }
       
    }
  },
  watch: {
    drawNo(newVal, oldVal) {
      // If detected changes on draw no, animate draw no changes
      if (newVal !== oldVal) {
      //   TweenLite.fromTo(
      //     this.$refs["header-draw-no"],
      //     1,
      //     {
      //       text: ""
      //     },
      //     {
      //       text: `${this.drawNo}`,
      //       ease: Linear.easeOut
      //     }
      //   );
         this.drawAnimation(1);

      }
    }
  }
};

export default gameHeaderMixin;
