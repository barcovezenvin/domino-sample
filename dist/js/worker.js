self.onmessage = function(e) {
  if (e.data[0] == "countdown") {
    var cntDown = e.data[1];
    var interval = setInterval(function() {
      cntDown--;
      self.postMessage(cntDown);
      if (cntDown <= 0) {
        clearInterval(interval);
      }
    }, 1000);
  }
};
